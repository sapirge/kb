<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;

$this->title = 'ProductSingle';
$this->params['breadcrumbs'][] = $this->title;


/*

$provider = new ActiveDataProvider([
    'query' => $product,
    'pagination' => [
        'pageSize' => 2,
    ],
]);



$provider = new ArrayDataProvider([
   'allModels' => $model->getAll()
]);
*/

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            'category_id'
        ],
        
    ]);
    
    ?> 


    <code><?= __FILE__ ?></code>
</div>
